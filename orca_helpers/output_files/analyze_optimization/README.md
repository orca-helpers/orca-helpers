# `analyze_optimization`
Plot convergence behavior for optimization runs.

## Description
This shell script uses the information from a geometry optimization output file and plots the convergence behaviour using gnuplot. Note that this functionality is also provided by [`universal_job_checker`](orca_helpers/output_files/universal_job_checker) which uses matplotlib for plotting.

## Usage
`analyze_opt.sh mygeometry.out`

Output:

![](gnuplot.png)


## Requirements
Tested with `ORCA ??`
- `gnuplot`

## Contributor
Contributed by Igor Schapiro
