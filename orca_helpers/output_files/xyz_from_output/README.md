# `xyz_from_output`
Create xyz file from output file.

## Description
Reads the *first* `CARTESIAN COORDINATES (ANGSTROEM)` block in the ORCA output file
and writes it to an xyz file. 
This is useful if you did not include the `! xyzfile` keyword.

## Usage
Run:

`xyz_from_output.py orca_output.out`

to create `orca_output.xyz`
or

`xyz_from_output.py -x custom_name.xyz orca_output.out`

## Requirements
Tested with `ORCA 4.2`
- `python3`
    - `argparse`
    - `re`
    - `pathlib`

## Contributor
Contriuted by Nico Spiller