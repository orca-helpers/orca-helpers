#!/usr/bin/env python3

import argparse
from pathlib import Path
import re

def get_xyz(output):
    'get xyz block from orca output file'


    with open(output) as f:
        xyz_lines = []
        parse = False
        for line in f:

            if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                parse = True
                next(f) # skip line with dashes
                continue

            if parse:
                if len(line.split()) == 0:
                    break
                else:
                    xyz_lines.append(line)

    return xyz_lines        

def write_xyz(xyz_list, xyz_file):
    'write list of lines to file'


    with open(xyz_file, 'w') as f:

        # first line: number of atoms
        n = len(xyz_list)
        f.write(str(n) + '\n') 

        f.write('coordinates extracted from orca output file\n') # second line: comment

        for l in xyz_list:
            f.write(l) # coordinates


def run():
    parser = argparse.ArgumentParser(
        description='Extract the coordineta block from ORCA output file and write to XYZ file')
    parser.add_argument('orca_out', metavar='ORCA_OUT', 
        help='ORCA output file')
    parser.add_argument('-x', metavar='XYZ_FILE', 
        help='XYZ file to be written. If not given, the program will use basename of ORCA_OUT.')
    args = parser.parse_args()

    orca = Path(args.orca_out)
    try:
        xyz = Path(args.x)
    except TypeError:
        xyz = Path(
            re.sub( '(\.mpi\d+)?\.out$', '.xyz', str(orca) ) )
    
    l = get_xyz(orca)
    write_xyz(l, xyz)

if __name__ == '__main__':

    run()

