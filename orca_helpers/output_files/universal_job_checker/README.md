# `universal_job_checker`
Check the status of many different kinds of calculations.

![](example.png)

## Description
This utility script will check the current state of any calculation given an
output file and return useful information about it, such as the job type, number of given cores,
atoms, charge, spin multiplicity, number of electrons and basis functions involved and much more.

## Usage
`orcajobcheck.py myoutput.out`

## Examples:
Geometry optimization
```
ORCA JobCheck Utility version 3.0 (Python3 version) 
-----------------------------------------------------------------------
File: opt.out 
ORCA version 4.2.1 ran serial job
ORCA terminated normally ( 0 days 0 hours 0 minutes 10 seconds 973 msec ) 

3 atoms. Charge: 0  Spin: 0.0  Contracted basis functions: 24
Initial orbitals via Guess
Optimization converged! in ( 4 iterations). YAY! 
FINAL OPTIMIZED ENERGY: -76.361230381332 
Integrated no. electrons: 9.999999967137 (should be 10 )
Do orcajobcheck output -p  to print optimized geometry
Do orcajobcheck output -plotgrad/-plotstep/-plotenergy  to plot gradient/step/energy using Matplotlib
```
DLPNO-CCSD(T) job:

```
ORCA JobCheck Utility version 3.0 (Python3 version) 
-----------------------------------------------------------------------
File: dlpno.mpi12.out 
ORCA version 4.2 ran 12 MPI-process job.
ORCA has not terminated with message and may still be running this job 

164 atoms. Charge: 0  Spin: 0.0  Contracted basis functions: 2823
Initial orbitals via Autostart
This is a single-point CC calculation
SCF is done. 
SCF energy is: -5403.66597800 
Running post-HF step 
Do orcajobcheck output -l N  to print last N lines.
Do orcajobcheck output -p  to print input geometry
```


Relaxed scan:

```
ORCA JobCheck Utility version 3.0 (Python3 version) 
-----------------------------------------------------------------------
File: scan.mpi4.out 
ORCA version 4.2.1 ran 4 MPI-process job.
ORCA terminated normally ( 0 days 3 hours 16 minutes 51 seconds 558 msec ) 

61 atoms. Charge: 1  Spin: 0.0  Contracted basis functions: 610
Initial orbitals via Guess
This is a Relaxed Surface Scan. Scanning Bond between atoms 56O and 55O . There will be 10 steps
Scanning from 1.47000000 to  2.50000000 (change is 0.114444  )
         Scan step       Scan parameter      Energy (hartree)        Rel. energy (kcal/mol)
====================================================================================================
         1                  1.470000              -3214.390988                  0.000000
         2                  1.584444              -3214.386591                  2.759280
         3                  1.698889              -3214.377632                  8.380787
         4                  1.813333              -3214.369032                 13.777456
         5                  1.927778              -3214.362205                 18.061742
         6                  2.042222              -3214.356259                 21.792855
         7                  2.156667              -3214.350513                 25.398455
         8                  2.271111              -3214.345039                 28.833106
         9                  2.385556              -3214.339945                 32.030179
        10                  2.500000              -3214.335382                 34.892952

Scan completed!
```

## Requirements
Tested with `ORCA 4.2`
- `python3`

## Known Issues
Please note that although this script was tested with `ORCA 4.2` it was written for an earlier version. This means that not all methods currently available in `ORCA` will be recognized by `universal_job_checker`, e.g. QM/MM jobs.

## Contributor
Contributed by Ragnar Björnsson
