# `single_point_from_optimization`
Create input files from optimization runs.

## Description
This shell script creates Orca input files for all converged geometry optimizations output files in the given path. Note that the script also parses input files of the same basename, if present. If not, the created input files will be empty except for the geometry.

## Usage
`SP-optgeo.ksh .`

for current path


`SP-optgeo.ksh ..`

for parent directory


`SP-optgeo.ksh [path/to/directory]`

for any given path.

## Requirements
Tested with `ORCA 4.2.1`

## Contributor
Contributed by Ragnar Björnsson
