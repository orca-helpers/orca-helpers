# `unit_conversion`
Convert xyz files from Angström to  to Bohr and vice versa.

## Description
Sometimes one needs to convert the units in xyz files from Bohr to Angström or vice versa.
In these cases the following shell scripts are useful.

## Usage

Bohrs to Angströms:

`bohr-to-ang molecule-bohrs.xyz`


Angströms to Bohrs:

`ang-to-bohr molecule-angs.xyz`

## Requirements
- `bash`

## Contributor
Contributed by Ragnar Björnsson
