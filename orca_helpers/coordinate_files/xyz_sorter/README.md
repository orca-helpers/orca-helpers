# `xyz_sorter`
Sorts the order in the xyz files of two similar structures.

## Description

If two xyz files contain the same molecule with similar structures, but the order of atoms is all mixed up,
you can use this script to unify the order of atoms. 

This can for example be helpful if you want to reuse GBW files from a calculation for a different crystal structure.

## Usage

`xyzSorter.py unsorted_molecule.xyz template.xyz sorted_molecule.xyz`

This will sort the order of atoms in `unsorted_molecule.xyz` to match that of `template.xyz` and write it to `sorted_molecule.xyz`.

## Requirements
- `python3`

## Known issues
The implementation is not very efficient and will only work for relatively small molecules.
For a faster implementation look [here](https://github.com/charnley/rmsd) (`--reorder` flag).

## Contributor
Contributor: Nico Spiller
