# Add block to Orca input
Adds a keyword block to multiple ORCA input files.

## Description
This shell script will add a given input block to all ORCA input files in the directory.
This can either be typed or pasted.

**Careful**: This script will not work if multiple simple input lines are present as the input block will be inserted after each of them.

## Usage
```
<Type (or copy-paste):>
%scf
maxiter 500
end
<Hit return twice>
```
## Requirements
- `bash`

## Known Issues
Only works for single single input line (i.e. just one line starts with `!`)

## Contributor
Contributed by Ragnar Björnsson
