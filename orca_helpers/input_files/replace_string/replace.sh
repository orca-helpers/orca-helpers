#!/bin/bash
# New replace. Acts only on files with extension.
if [ "$1" == "" ]
then
echo " Script to replace strings in a set of files (e.g. ORCA inputfiles) in folder. "
echo " usage: replace [subst1] [subst2] [filesuffix]"
echo " For example: replace b3lyp mp2 .inp"
echo " This acts on all .inp files and replaces b3lyp with mp2"
exit 1
elif [ "$3" == "" ]
then
exit 1
else
file=$3
for i in $(ls *$file )
do
sed -i "s:$1:$2:g" $i
echo "Replacing $1 with $2 in file $i"
done
fi
