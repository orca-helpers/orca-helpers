# `replace_string`
Replace a string with another in multiple files.

## Description
This shell script replaces each instance of a given string with another given
string in the current directory and acts on all files with a given extension.

## Usage

To replace B3LYP with MP2 in all `.inp` files do, e.g.:

`replace.sh B3LYP MP2 .inp`


To delete a given string from all `.inp` files do, e.g.:

`replace.sh slowconv "" .inp`

## Requirements
- `bash`

## Contributor
Contributed by Ragnar Björnsson
