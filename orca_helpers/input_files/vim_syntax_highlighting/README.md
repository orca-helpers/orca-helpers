# `vim_syntax_highlighting`
Add some very basic highlighting for orca input files in vim. 
![](example.png)

## Description
Most code structures are recognized via the a file in the `~/.vim/syntax` folder.
All files ending with `*.inp` are recognized as ORCA files

## Installation
1. Place `.vim/ftplugin/orca.vim`at `$HOME/.vim/ftplugin/orca.vim`
2. Place `.vim/syntax/orca.vim` at `$HOME/.vim/syntax/orca.vim`


## Known issues: 
- everything after the `* xyzfile [] []` line will be of same color
- comments do not close on `#` (but they do in ORCA)

## Requirements
With vim version 8.0 no further settings were required.

## Contributor
Contributed by Nico Spiller
