# `plot_with_pymol`
PyMol script for plotting

## Description 
This script will simplify plotting orbitals and densities with PyMol. Both can be handled with a single line in the PyMol command prompt in addition to saving the created figures to a file.

## Usage
Load the script file from the PyMOL console:  

`run [path-to-script]/dens_plot.py`

Plot a density/orbital given as cube file (needs to be present in current directory)

`show_dens mydens.cube`

The isovalues default to 0.02 and -0.02 and the default colors are orange and cyan. This can be changed like so:

`show_dens mydens.cube, 0.03 -0.03, red blue`

For mapping the ESP onto the density do:

`map_esp mydens.cube, esp.cube`

In order to save a figure to disk use the `save_dens` command with the same arguments as above, e.g.:

`show_dens mydens.cube, 0.03 -0.03, red blue`

To display all available commands:

`dens_plot_help`

## Requirements
Tested with `ORCA 4.2`

Tested with `PyMol 2.4`

## Contributor
Contributed by Felix Plasser
