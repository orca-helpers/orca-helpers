# `create_cube`
Wrapper for calling `orca_plot` utility. 

## Usage
`create_cube.py orca.gbw --alpha 1,2,3 --beta 0 --grid 50`

Ranges are also supported:

`create_cube.py orca.gbw --alpha 3,5,9,12-45`

## Description
This short python script can generate a large number of `cube` files (or anything `orca_plot` can do) with a single line of input. 

## Requirements
- `orca_plot`
- `python 3.6`

## Contributor
Contributed by Nico Spiller
