![](images/orca_placeholder.jpg)

[[_TOC_]]

# ORCA helpers
A collection of small programs and scripts for the
[ORCA quantum chemistry program package](https://orcaforum.kofo.mpg.de/app.php/portal).

All quantum chemists are writing more or less elaborate code to process 
the huge amount of data created during quantum chemical calculations. 
If you have written some piece of software that could benefit your
fellow researchers, please share it here! 
Otherwise, have fun digging around and get inspired by the workflow of others. 

# Contribute
If you find any bugs, want to improve the code, or contribute a new helper, you can
- create a gitlab account (when you request access at support@gwdg.de, please CC orca-helpers@kofo.mpg.de)
- write an email to orca-helpers@kofo.mpg.de (note, that the maintainers are not the authors of the helpers)

# Guidelines
Each program should run as a standalone utility with as few dependencies as possible.
Create an individual folder for each program.

The `README.md` must include at least:
```
# `folder_title`
One-sentence description (to be included in the main `README.md`)

![](image/example.png) (picture, illustration, screenshot etc. to be included in the main `README.md`)


## Description
more elaborate description

## Usage
explanation how to use the program

## Example
if supplied

## Requirements
Tested with `ORCA <version>`
- programs such as `python3` etc
- additional ORCA keywords necessary

## Contributor
Contributed by <your_name>
```

# List of helpers
## [`coordinate_files`](orca_helpers/coordinate_files/)
### [`unit_conversion`](orca_helpers/coordinate_files/unit_conversion)
Convert xyz files from Angström to  to Bohr and vice versa.

<img src="orca_helpers/coordinate_files/unit_conversion/example.png"  width="600">

### [`xyz_sorter`](orca_helpers/coordinate_files/xyz_sorter)
Sorts the order in the xyz files of two similar structures.

<img src="orca_helpers/coordinate_files/xyz_sorter/example.png"  width="600">

## [`gbw_files`](orca_helpers/gbw_files/)
### [`create_cube`](orca_helpers/gbw_files/create_cube)
Wrapper for calling `orca_plot` utility. 

<img src="orca_helpers/gbw_files/create_cube/example.gif"  width="600">

### [`plot_with_pymol`](orca_helpers/gbw_files/plot_with_pymol)
Plot orbitals and densities with PyMol

<img src="orca_helpers/gbw_files/plot_with_pymol/example.png"  width="600">

## [`input_files`](orca_helpers/input_files)
### [`add_block_to_input`](orca_helpers/input_files/add_block_to_input)
Adds a keyword block to multiple ORCA input files.

<img src="orca_helpers/input_files/add_block_to_input/add.gif"  width="600">

### [`replace_string`](orca_helpers/input_files/replace_string)
Replace a string with another in multiple files.

<img src="orca_helpers/input_files/replace_string/replace.gif"  width="600">

### [`vim_syntax_highlighting`](orca_helpers/input_files/vim_syntax_highlighting/)
Add some very basic highlighting for orca input files in vim. 

<img src="orca_helpers/input_files/vim_syntax_highlighting/vim_syntax.gif"  width="600">

## [`output_files`](orca_helpers/output_files/)
### [`analyze_optimization`](orca_helpers/output_files/analyze_optimization)
Plot convergence behavior for optimization runs.

<img src="orca_helpers/output_files/analyze_optimization/gnuplot.png"  width="600">

### [`led_analysis`](https://github.com/iharden/orca_led) (external)
Prints main results from Local Energy Decomposition analysis and decomposes the interaction energy between the monomers.

<img src="images/external_orca_led.gif"  width="600">

### [`loewdin_analysis`](orca_helpers/output_files/loewdin_analysis)
Plots Loewdin population of molecular orbitals.

<img src="orca_helpers/output_files/loewdin_analysis/example/a-cntrb-a.png"  width="600">

### [`loewdin_plotter`](orca_helpers/output_files/loewdin_plotter)
Plots specific Loewdin population of specific orbitals.

<img src="orca_helpers/output_files/loewdin_plotter/example/example.png"  width="600">

### [`single_point_from_optimization`](orca_helpers/output_files/single_points_from_optimizations)
Create input files from optimization runs.

<img src="orca_helpers/output_files/single_points_from_optimizations/SP_from_opt.gif"  width="600">

### [`universal_job_checker`](orca_helpers/output_files/universal_job_checker)
Check the status of many different kinds of calculations.

<img src="orca_helpers/output_files/universal_job_checker/jobcheck.gif"  width="800">

### [`print_ir`](orca_helpers/output_files/print_ir)
Create IR spectrum from output file.

### [`print_uv`](orca_helpers/output_files/print_uv)
Create UV/Vis spectrum from output file.

### [`print_excitated_states`](orca_helpers/output_files/print_excited_states)
Collect excited state data from output file into table.

### [`xyz_from_output`](orca_helpers/output_files/xyz_from_output)
Create xyz file from output file.

<img src="orca_helpers/output_files/xyz_from_output/xyz_from_out.gif"  width="600">

## [`pdb_files`](orca_helpers/pdb_files)
### [`pdbman`](orca_helpers/pdb_files/pdbman)
Analyzes and modifies PDB files for QM/MM calculations.

<img src="orca_helpers/pdb_files/pdbman/example/pdbman.gif"  width="800">
